﻿# Minecraft 2D

![Java CI with Gradle](https://github.com/Over-Run/Minecraft2D/workflows/Java%20CI%20with%20Gradle/badge.svg?branch=alpha)  
![GitHub search hit counter](https://img.shields.io/github/search/Over-Run/Minecraft2D/goto)  
![GitHub release (latest by date including pre-releases)](https://img.shields.io/github/v/release/Over-Run/Minecraft2D?include_prereleases)

Welcome to Minecraft 2D. This is a sandbox game.  
欢迎来到Minecraft 2D的世界！这是一个沙盒游戏。

This game is publishing on [itch.io](https://squid233.itch.io/minecraft2d), too.

Some assets are producing by Mojang Studios.

## 运行时环境 Runtime Environment

最低要求JRE11，低于此版本会导致不能正常启动

## 预览图 Preview

0.1.0.40-pre-alpha  
![0-1-0-40-0](/img/0-1-0-40-0.png)![0-1-0-40-1](/img/0-1-0-40-1.png)

## 使用方法 Use Guide

1. go to [itch.io](https://squid233.itch.io/minecraft2d) to download。  
首先，到 [此处](https://over-run.github.io/mc2d/latest.html) 选择版本并下载。
2. Write a startup script and put in your directory.  
编写一个启动脚本并放到文件夹里。

### The startup script

```batch
@echo off
java -jar *.jar
pause
```

## 贡献 Contributing

此项目仍处于初始阶段。我们希望您能为我们做出贡献，哪怕只是1个issue或1个PR。

See [CONTRIBUTING.md](CONTRIBUTING.md)

## 其它 Other

// Join our Discord channel just time! [![discord](https://img.shields.io/discord/751804389718753421)](https://discord.gg/ydYzTKV)

现在就加入我们的QQ~~吹水~~群！[![Java 开发交流群](https://pub.idqqimg.com/wpa/images/group.png)](https://qm.qq.com/cgi-bin/qm/qr?k=efwa2cjVSs-S_UorWELGd45SPTJBTGV6&jump_from=webapi)

看看[Fandom Wiki](https://minecraft2d.fandom.com/zh/wiki/)!

View this game on our [official site](https://over-run.github.io/)!  
在 [官方网站](https://over-run.github.io/) 上看我们的游戏!

## Other Edition

- [Kotlin Edition](https://github.com/Over-Run/Minecraft2D-Kotlin)
- [Python Edition](https://github.com/QWERTY770/Minecraft-2D-Python/)

## Minecraft2D vs. Minecraft

|   | Minecraft2D | Minecraft |
|---|-------------|-----------|
| Free | √ | |
| Java Version | 11+ | 7/8+ |
| Mods | | √ |
| Multiplayer | | √ |
| I18n | √ | √ |

## Credits

Thanks for AliCloud
