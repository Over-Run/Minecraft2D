# 贡献
由于该项目仍处于初期开发阶段，您可以帮助我们改进代码。

## Issues
Issues 是帮助我们的最简单的方法。您可以提出问题，也可以提供一个想法。

### 对于玩家
对于玩家，您需要提供尽量详细的信息。

#### Bug
出现bug后，请截图说明情况，并留下出现bug时所用的操作系统、Java版本、游戏版本以及其他必要信息。

#### 新的特性 Or 想法
如果您有一个新的想法，不要犹豫！立即告诉我们，我们将会在内部进行讨论并决定最后是否加入这个特性。

当然，您也可以加入讨论。只需加入 [Our Discord Channel](https://discord.gg/ydYzTKV) 或 [QQ群](https://qm.qq.com/cgi-bin/qm/qr?k=Nnh75LW0PJysy9rHMF6EOxAwBBjBN6mt&jump_from=webapi) 。
